﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    public float maxHealth = 10;
    public float health = 10;

    private MovePlayer _movePlayer;

    public void Start()
    {
        _movePlayer = GetComponent<MovePlayer>();
    }

    public void HealDamage(float amount)
    {
        if (!enabled)
        {
            return;
        }

        if (maxHealth < health + amount)
        {
            return;
        }

        health += amount; 
    }

    public void TakeDamage(float amount)
    {
        if (!enabled)
        {
            return;
        }

        health -= amount;

        if (health <= 0)
        {
            Die();
        }
    }

    public void Revive()
    {
        enabled = true;
        health = maxHealth;
        _movePlayer.enabled = true;
    }

    public void Die()
    {
        Debug.Log("Died");
        enabled = false;
        _movePlayer.enabled = false;
        var spawner = GameObject.Find("Respawner").GetComponent<SpawnController>();

        spawner.RespawnPlayer();
    }
}
