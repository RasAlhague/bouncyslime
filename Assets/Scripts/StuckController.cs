﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StuckController : MonoBehaviour
{
    public int maxCount = 100;
    public int stuckCount = 0;

    private void OnCollisionStay2D(Collision2D collision)
    {
        stuckCount++;

        if (stuckCount < maxCount)
        {
            return;
        }

        var healthComponent = collision.gameObject.GetComponent<HealthController>();

        if (healthComponent != null)
        {
            healthComponent.TakeDamage(10);
            stuckCount = 0;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        stuckCount = 0;
    }
}
