﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public Vector2 playerSpawn;
    public GameObject player;

    public void RespawnPlayer()
    {
        player.transform.position = playerSpawn;
        player.GetComponent<HealthController>().Revive();
        player.GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
    }
}
