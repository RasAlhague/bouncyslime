﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePlayer : MonoBehaviour
{
    private Rigidbody2D rgbd2D;
    private CapsuleCollider2D capsuleCollider2D;

    [SerializeField] private LayerMask groundMask;
    [SerializeField] private float extraHeight = 0.1f;

    public float speed = 1;
    public float maxDistance = 10;

    public void Start()
    {
        rgbd2D = GetComponent<Rigidbody2D>();
        capsuleCollider2D = GetComponent<CapsuleCollider2D>();
    }

    private void Update()
    {
        Vector3 mouseWorldPosition = Utils.GetMouseWorldPosition();

        var distance = Vector2.Distance(transform.position, mouseWorldPosition);

        if (distance >= maxDistance)
        {
            distance = maxDistance;
        }

        if (IsGrounded() && Input.GetMouseButtonUp(0))
        {
            Launch(speed * distance);
        }

        Debug.DrawLine(mouseWorldPosition, transform.position, Color.green);
    }

    private bool IsGrounded()
    {
        var raycastHit = Physics2D.BoxCast(capsuleCollider2D.bounds.center, capsuleCollider2D.bounds.size, 0f, Vector2.down, extraHeight, groundMask);

        Color rayColor;

        if (raycastHit.collider != null)
        {
            rayColor = Color.green;
        }
        else
        {
            rayColor = Color.red;
        }

        Debug.DrawRay(capsuleCollider2D.bounds.center + new Vector3(capsuleCollider2D.bounds.extents.x, 0), Vector2.down * (capsuleCollider2D.bounds.extents.y + extraHeight), rayColor);
        Debug.DrawRay(capsuleCollider2D.bounds.center - new Vector3(capsuleCollider2D.bounds.extents.x, 0), Vector2.down * (capsuleCollider2D.bounds.extents.y + extraHeight), rayColor);
        Debug.DrawRay(capsuleCollider2D.bounds.center - new Vector3(capsuleCollider2D.bounds.extents.x, capsuleCollider2D.bounds.extents.y + extraHeight), Vector2.right * (capsuleCollider2D.bounds.extents.x * 2), rayColor);

        return raycastHit.collider != null;
    }

    private void Launch(float force)
    {
        Vector2 dir = (Utils.GetMouseWorldPosition() - transform.position).normalized * -1f;
        rgbd2D.velocity = dir * force;
    }
}
